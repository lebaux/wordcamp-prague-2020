# Notes



https://docs.google.com/spreadsheets/d/1MvKhBoJWmZRERok-9ukcDyRbh6Y9v43mer7K1dMkKVA/edit#gid=2087948626 program

https://docs.google.com/document/d/1af7AlkdNHurBkfO5amT_wZqXfYLwUf5GSm6NgTQxdFc/edit#heading=h.jq4ixaw55hmg speaker questions

https://joinmastodon.org/ color scheme



### Presentation tools

https://www.brainpickings.org/2012/12/20/writing-that-works-roman/ reminder how to make a presentation

https://yhatt-marp-cli-example.netlify.com/#7

https://github.com/hakimel/reveal.js

https://www.chenhuijing.com/talks/

https://www.elated.com/html5-audio/ audio button on first slide

https://codepen.io/websitebeaver/pen/vKdWxW



# Selected sources

## To follow

https://twitter.com/DeepCrawl

https://twitter.com/peckadesign

https://twitter.com/LinkiCZ

## Good reads

1. How to Scale Organic Traffic (Without Writing a Million Blog Posts) https://cxl.com/blog/scale-organic-traffic/
2. You should write 3000+ word articles. Avoid 300-600. https://www.semrush.com/blog/anatomy-of-top-performing-articles/



https://twitter.com/CyrusShepard/status/1168567157539557380 **If an SEO tool has an affiliate program, 10/10 times it wins every "Best SEO Tool" comparison post**

https://twitter.com/dlberes/status/1201880243809783808 affiliate is ruining reviews





## Cut content get paid

https://twitter.com/pilhofer/status/1139537199073058816 The Guardian cut its weekly story production by *one third*... and traffic went up, said [@KathViner](https://twitter.com/KathViner)

https://dennikn.sk/1741628/list-predplatitelom-rok-2019-bol-pre-dennik-n-rekordny/?ref=tema 

https://twitter.com/Simindr/status/1192096090952216576 **dennikN 44k predplatitelov**, obdobne zaraba holandsky https://thecorrespondent.com/about

https://www.businessinsider.com/2019-media-layoffs-job-cuts-at-buzzfeed-huffpost-vice-details-2019-2 lost jobs in media, babisarna

ghost is making money, chargd is making money

https://twitter.com/semrush/status/1193854135323545600 removing content to increase rankings (2018)

#dennikn #enko #bloomberg #hrabiš







Your Analytics Data Isn't Real and It's Only Getting Worse" [http://bit.ly/2Wr1MfP](https://t.co/csR204OIiJ?amp=1) It's been a long time since I wrote something. https://twitter.com/iPullRank/status/1124025636627066880

++gmb







Sitekit https://twitter.com/10up/status/1141396853025464320 2019/07/19

Pagerank explained http://www.seobythesea.com/2018/04/pagerank-updated/

Ahrefs vs SEMRush https://www.getcredo.com/ahrefs-vs-semrush/ (affil)

https://twitter.com/MSFTAdvertising/status/898208047578849280

Complete guide to mobile first indexing https://www.mariehaynes.com/mobile-first-indexing/



Fitness Industry in 2019

Coach to Client: “Stop looking for the quick fix. This takes YEARS! Be patient & get 1% better every day.

The Same Coach to Another Coach: “Just bought that 6-Figures in 6-Months Online Mastermind! Their 3-Step Instagram Growth Hacker looks SICK!”

https://twitter.com/SyattFitness/status/1146828083342843905



Wow, guess that’s Giphy’s death sentence? Not completely surprising but still shrewd to see. [#answerengine](https://twitter.com/hashtag/answerengine?src=hashtag_click)

https://twitter.com/Kevin_Indig/status/1144431027978293248



## Too complicated or too generic for default WP user

Typical SEO Article that is of low value for typical WP user. https://twitter.com/Builtvisible/status/1148189238627844098 Demonstrating Expertise, Authoritativeness and Trustworthiness through your site is of increasing importance, especially for ‘Your Money or Your Life’ (YMYL) websites. Here are some tips for how websites – particularly YMYL websites – can improve E-A-T. https://buff.ly/2Ryhsw1 

V zásade je tam toto viacej od Builtvisible https://twitter.com/PavelUngr/status/1144231164678422529

Over the past couple of months, the DeepCrawl team have been thinking about ways to improve the efficiency of SEO reporting efforts, using tools like Google Data Studio and Zapier, and I’m excited to share some of the fruits of our labour in this post. https://www.deepcrawl.com/blog/best-practice/automating-seo-reporting/

Vut also simple looking content like, https://twitter.com/hootsuite/status/1139989750985740288 but who has time for that

The 5-step #SEO 'Skyscraper' technique to steal your competitors' links:
Heavy check markDetermine keywords
Heavy check markFind content you can beat
Heavy check markCreate improved content
Heavy check markOutreach & link building
Heavy check markRinse & repeat

Check out the infographic for more detail: http://bit.ly/2IMEVWk via 
@socialmedia2day

https://twitter.com/semrush/status/1143989315770433541



speaking of semrush https://twitter.com/semrush/status/1140130131169730560 50 email ideas... 170+ inspections points for SEO. https://twitter.com/semrush/status/1139823870402322432



https://twitter.com/semrush/status/1143507135759826945 leads to unauthoritative source, that leads to hubspot that leads to study from 2015 that is 404. And I saved this and lot of other stats are parrot are garbage. But I found it https://blogs.adobe.com/creative/files/2015/12/Adobe-State-of-Content-Report.pdf study is based on 1000 users over 18+ using 1+ digital device. Even hubspot is not watching their 404s! V roku 2018 mali obrat 518M, ako môže mať malý webík na WP nástroje, keď ani hubspot ich nemá?



Consider this tweet:

https://twitter.com/semrush/status/1142827913013858307

Ďalší trash source https://www.reliablesoft.net/long-tail-keywords/ vybratý SEMRush. Oh yeah, Neil Patel. So follow the link... aaand https://neilpatel.com/blog/long-tail-keywords-seo/ to completely useless blog https://www.advancedwebranking.com/blog/how-to-identify-long-tail-keywords-for-your-seo-campaign/ but the dude has 25k followers and it is verified! This is article from 2013, čo je ako pred kristom. SEMRUSH to tweetlo 23.6.2019.



### Voice search

https://brodieclark.com/stop-using-comscores-2020-voice-search-stat/

https://twitter.com/aleyda/status/1202576322725216257 3,5b searches, 50% on mobile but not voice

https://magecomp.com/blog/seo-in-2020/ - bad prediction, voice search

https://twitter.com/DigitalMktgTool/status/1214955619532394497 real share of voice assistant (500, in 2020)

https://www.reddit.com/r/bigseo/comments/es0mi1/what_are_your_top_5_biggest_seo_myths_that_tick/ bad voice









## Epic content turned turd

https://freshchalk.com/blog/150k-small-business-website-teardown-2019

https://freshchalk.com/blog/150k-small-business-website-teardown-part-two

https://freshchalk.com/blog/150k-small-business-website-teardown-part-three

vs https://freshchalk.com/blog and they guy haven't tweeted this year



## Gamification selected

https://www.searchenginejournal.com/google-web-dev-rankings/293291/



















## Local search, Google Small Business

https://localseochecklist.org/

https://www.localsearchforum.com



The first two pages of a google search almost never surface blogs or personal websites anymore.  Every worthwhile question you could think to ask has been anticipated and occupied by search-engine-optimized by assholes.  The internet hasn't been free for a really long time.

https://twitter.com/girlziplocked/status/1207036521565577220 start with this, JohnMu liked it.



https://twitter.com/rustybrick/status/1140967469907156992 Check out the interface to let Google Assistant, Duplex, book reservations for you.



https://twitter.com/rustybrick/status/1122827404278411264 gmb charging for service (on top of the ads, monthly payment) https://www.seroundtable.com/google-my-business-monetize-27482.html



If you advertise with google, you can have "google" guaranteed badge. No other way to get it. https://twitter.com/tomwaddington8/status/1146826988516196352

This was previous Google Tweet https://twitter.com/GoogleSmallBiz/status/1142104573215162369 Google makes mistakes too.



Using GMB "shortname" leads to bug removing reviews or listings alltogether https://twitter.com/YoungbloodJoe/status/1147095838277156864

Amazon is even worse, sellers are bribing users with cash to get 5-star reviews https://twitter.com/GarrettSmith/status/1147141526704795648









## Start with an idea

Dollar shave club

https://web.archive.org/web/20130930121352/http://comissioncontent.com/the-myth-of-dollar-shave-clubs-viral-video/ dollar shave club, use it in pararel to dropbox?

https://venturebeat.com/2012/03/06/dollar-shave-club/

https://techcrunch.com/2016/07/22/why-did-unilever-pay-1b-for-dollar-shave-club

++dropbox







## Why have a website

**Oatmeal, and how facebook gatekeeps content**

https://www.reddit.com/user/System32Comics/comments/czhckk/how_not_to_run_a_website/ + facebook from oatmeal

https://twitter.com/Oatmeal/status/923244873028734976/photo/1 reaching people on the internet

https://theoatmeal.com/comics/reaching_people

https://www.facebook.com/theoatmeal/photos/a.10150413121115078/10159581727300078/?type=3&theater (his comment under)

https://www.reddit.com/r/funny/comments/d6t5ga/are_you_sure/





## There is Wix, Medium and others doing a good job

https://twitter.com/TheCoolestCool/status/1219824344689496065 **wix might reach 1MM in revenue by 2021**





## Snippets being... bad?

https://twitter.com/CyrusShepard/status/1150834150670139393 zero clicks over 50% in 2019

https://twitter.com/Marie_Haynes/status/1220382611392139264 having a featured snippet might not help your CTR







## How to start with SEO (tools and process)

https://twitter.com/milanpichlik/status/1125305910895706112 **affil a SEO a obnova domény**















# Random, boiling pot of ideas

https://www.zatkovic.cz/aks-na-maximum-automatizace/

https://yoast.com/live-indexing-bing-google-yoast-seo/ yoast claimed at YoastCon Live indexing will be exclusive for them, while JohnMu denied it on twitter.







## Twitter

https://twitter.com/brodieseo/status/1232521564635054081 how GMB is inflated

https://twitter.com/davidmihm/status/1222896346971045891 GMB views are inflated

https://twitter.com/JoyanneHawkins/status/1217149078838108160 again GMB insights being unreliable

https://twitter.com/JoyanneHawkins/status/1215690017886101504 real impact of fake google businesses —"I looked on Google Maps because I wanted to use somebody local that had a good reputation," he said.



Google Maps removed 4 million fake business profiles and 75 million reviews, 10 million photos and 3 million videos in 2019. https://www.seroundtable.com/google-removed-4-million-fake-business-75-million-local-reviews-29037.html + https://twitter.com/rustybrick/status/1230930436722917380





https://twitter.com/AdrianaCerna/status/1230776821760876544 **sputnik**

https://twitter.com/badams/status/1227146170058903554 google getting sued because GMB is not fair holidays rental (hotels expedia trivago)







https://twitter.com/glenngabe/status/1219621903150845953 **invest in bing, they do a lot for you for free.**

https://twitter.com/Miriam_Ellis_/status/1219667886513373184 **google bought pointy, SMB store solution to adverstise your small inverotiny online**

https://twitter.com/tkrause/status/1219293840156348416 **don't touch your web if you don't have to. Drop in rankings.**



https://twitter.com/nicolaihelling/status/1218124543165968384 GMB how it looks in germany vs us.

https://twitter.com/Marie_Haynes/status/1217431139436068866 **this is the problem, GMB replying to employee of google with auto reply**

https://mujsvetmarketingu.cz/obsahovy-marketing-kompletni-pruvodce/ **you shouldnt write about SEO in CZE. You are 30 likes at best and with hours of effort.** 



https://twitter.com/semrush/status/1216348605721796608 **neat graph, about SEO and social and mail.**

https://twitter.com/rustybrick/status/1215614799926714370 Google says don’t manipulate the date on your evergreen content, it is evergreen for a reason (hint wpbeginner)





https://twitter.com/ColanNielsen/status/1215051789705400322 **GMB in between**

https://twitter.com/tomwaddington8/status/1205990958258704391 GMB servers locations

https://twitter.com/rustybrick/status/1207278168798846978 **GMB request quote from multiple providers**



https://twitter.com/YoungbloodJoe/status/1214637935611564032 **GMB outsouced to people who are not google yet use @google email ** https://www.seroundtable.com/google-ads-teleperformance-poaching-clients-28794.html

https://twitter.com/BubblesUp/status/1213479789354528768 **days of 5 stars GMB review are gone**

https://twitter.com/marypcbuk/status/1207386944935809025 google as ads intermidiator





https://twitter.com/searchliaison/status/1201549327144898567 GMB neural matching

https://twitter.com/sherrybonelli/status/1200774867307651073 GMB google doing whatever they want, by moz







https://twitter.com/Rarst/status/1200318473148289029 WordPress take on capitalism is “give users everything for free, money are gross, unless it’s Automattic making them”

https://twitter.com/__nca/status/1197509726071906305 GMB about to use own booking system (nov 2019)

https://twitter.com/JoyanneHawkins/status/1196897541238996992 GMB ads

https://twitter.com/rustybrick/status/1194268864286208000 you can't call GMB support anymore

https://twitter.com/LinkiCZ/status/1194190516780437504 SERP snippets

https://twitter.com/dohertyjf/status/1194029271435464706 speed is a king is garbage

https://twitter.com/screamingfrog/status/1193976081029697538 google bullying slow websites

https://twitter.com/CyrusShepard/status/1193907516339609600 **emotion is not a ranking factor but it should be**

https://twitter.com/DeepCrawl/status/1193853883954745344 social media are not a ranking signal, because it is hard to keep up (ok lol)

https://twitter.com/SEOloger/status/1193854916122406912 how much to charge as SEO in CZE



https://twitter.com/neologyc/status/1193663371142422529 google is just advertising space, where they just make your own website obsolete

https://twitter.com/jonoalderson/status/1192380499706155008 on wp speed with Jono



https://twitter.com/glenngabe/status/1160528666763366401 25% of Yelps money comes from chains, rest from single location businesses.







https://twitter.com/CyrusShepard/status/1192152280952295425 **state of SEO Twitter**

https://twitter.com/rustybrick/status/1189150708022632449 aaaaand google

https://twitter.com/dineshkaku/status/1183553287166939136 aaaaand john

https://www.youtube.com/watch?v=iReQtzFqEMs&feature=youtu.be **great deep dive in Google's Quality Raters' Guidelines** 

https://twitter.com/RichLawther/status/1192044437700038656 screaming frog tutorial for slow pages



https://twitter.com/semrush/status/1178597859123515392 **there is too much content to write**

https://twitter.com/lilyraynyc/status/1177634761600389122 why never update again, google does whatever it wants

https://twitter.com/lilyraynyc/status/1171757461759561728 nofollow is a "hint" (google says pagerank does not matter, yet they take action to open the juice flow)

https://twitter.com/dumitru/status/1170967238586884096 The Apache Software Foundation Earns $246.000/Year Selling Dofollow Links for SEO, JohnMu says it is not doing much... so why they do it?





https://twitter.com/izzionfire/status/1160920277703897092 it depends

https://twitter.com/riseatseven/status/1157180268932939777 disavow of 250k links







https://twitter.com/GuideTwit/status/1151769903067750401 SMB spam, 2 years and counting.

https://twitter.com/brightlocal/status/1101129845294223360 google introduced tool to report listing only on 2019/02







https://twitter.com/AndreyAzimov/status/1201090999298117632 remove the free plan (money) maybe relay to AIO SEO and GADWP sale?









### Seznam

https://twitter.com/milanpichlik/status/1206138067914035200 8 slots for 1 url

https://twitter.com/PavelDrabek/status/1172108970682650624 25% search share

https://twitter.com/Lukaaashek/status/1151102802699014144 webmaster api pro seznam

https://twitter.com/IvoKylian/status/1219961733596569601 seznam... has still isssues, big ones. PPC query!



### Automattic

https://twitter.com/post_status/status/1161062019866533888 purchased tumblr for 3M, compared to 1.1B Yahoo paid for it. Ban on porn.

https://twitter.com/somospostpc/status/1161024458460712962 same

https://twitter.com/lebaux/status/1159443582723530752 jetpack drama

https://twitter.com/explorionary/status/1200100413523054592 Yoast Vs Yoast

https://twitter.com/mehul_gohil0810/status/1110488258587308033



### Stats

https://www.similarweb.com/corp/reports/2020-digital-trends-report/

- 8% rise of the internet trafic from 2018
- Top100 websites, 223B visits per month.
- Destkop decreasing, down 3.3%, mobile over same period of time 30.6%+
- Attention span dropped to 49s over 3 years (758 to 709)
- Big are getting bigger: Top10 websites had 167.5B visist, 10.7% increase. Remaining 90 from top100 saw only 2.3% increase. Facebook lost 8.6% YoY, Yahoo 33.6%.
- Porn has 86.17% share on mobile vs Desktop. Same with Gambling, Food, Pets, Health, Social, Sports, Lifestyle, Vehicles... Jobs.
- Czech republic has most visits to top 100 vbsites per capita on the planet, 1231 visits per capita. Next is Canada, Netherlands and Poland, only then USA.
- Online shopping is Growing in Every category, fueled by amazon that grew 69% over last 3 years.
- **OTA lost 9-5% and metasearch 6.6% over last 2 years.**
- Google Hotels is booming, YoY at 67%, flights plateu
- Hotels traffic incrased over 600% ajd converted 550% more YoY
- Flights 16% and 24%
- News sites are losing, top100 media are down 5.4 YoY and 7% since 2017
- Display Ads making a comeback, notably on buzzfeed with 23.9% YoY

https://twitter.com/CyrusShepard/status/1146151791131811842

- Only 30% of small businesses would recommend their current SEO
- 50% say they need more SEO training
- 27% find SEO confusing



https://www.sistrix.com/blog/copyright-law-for-press-publishers-in-the-eu-journalistic-content-often-irrelevant-for-google/

- Only 0.11% of commercial search queries can be characterised as journalistic in nature
- 2.65% of search queries can be characterised as journalistic
- 10.18% of all results from journalistic domains



https://mailchimp.com/annual-report/stats/signups-per-day/

- Mailchimp has 11k signups per day



https://www.slideshare.net/JoyHawkins/mozcon-2019-factors-that-affect-the-local-algorithm-that-dont-impact-organic SMB reviews count only for 15.4% of SMB ranking in local pack, removed reviews seem to still count towards average.



https://twitter.com/lilyraynyc/status/1150808499888148481 Google saw a 16% growth in ad CTR since switching the “Ad” icon from green to black (and making it virtually indistinguishable from organic search results) 

https://twitter.com/jroakes/status/1216817524442091520 **google ads in serp change over the years.**



https://twitter.com/PavelUngr/status/1150780819054759939 **301 přesměrování převede 100 % link-juice, pouze pokud přesměrování vede přímo na stránku s 200, pokud se tematicky velmi blíží přesměrované stránce**



https://moz.com/blog/how-low-can-number-one-go-2020 **back in 2013, we analyzed 10,000 searches and found out that the average #1 ranking began at 375 pixels (px) down the page. The worst case scenario, a search for "Disney stock," pushed #1 all the way down to 976px.**







https://twitter.com/dliciousbrains/status/1200818269520834562 35% of WP devs code for more than 15 years

https://twitter.com/SUPERKODERS/status/1197808824842964992 cloud typography can have 20% impact on bounce rate

https://twitter.com/ahrefs/status/1180901798279831553 **90.88% pages get no organic traffic (920M websites, 2019)**

https://twitter.com/eMarketer/status/1215619512814403584 linkedin counts for 1/3 of all users of social networks in USA

https://twitter.com/pootlepress/status/1171803484590628864 avada theme still making 84k on a good week

https://twitter.com/eMarketer/status/1171030726713757696 GMB 93.4% of customers read reviews before making a purchase. Yet, you can't contact google about it. JohnMu advice is useless (in tweets)

https://ahrefs.com/blog/how-to-get-on-the-first-page-of-google/

only 0.78% people clicks on links on second page of SERP. Therefore, do not go for "SEO konzultant praha".

https://www.collabim.cz/statistiky.html resource

https://twitter.com/randfish/status/1161318597597126656 **Less than half of searches on Google now result in a click.** 





https://www.buildersociety.com/threads/new-seo-insight-into-why-some-new-sites-seem-blessed-while-others-are-doomed.4869/ johnmu admitting that this is THEORY. **"You guys insist content is the most important ranking factor, but we only see new content ranking for big terms if they're published on old and powerful domains. That means time is a ranking factor. Change my mind."** 







https://www.matteoduo.com/upselling-advertising-crossselling-wordpress-org-dashboard/ wordpress jetpack special treatment, ads

https://digwp.com/2016/09/stop-ruining-wp-admin-area/ older ads

https://affiliateinsights.com/expired-domains-for-seo/ expired domains for SEO, PBN



https://www.rankranger.com/blog/how-accurate-is-google the problem with accuracy in google



https://www.reddit.com/r/bigseo/comments/d5f9pb/google_removiung_selfserving_reviews_for/ google removing self serving reviews



[Don't ask yourself: "How can I make money?" Ask yourself: "What problems can I solve?"](https://www.reddit.com/r/Entrepreneur/comments/evzxqv/dont_ask_yourself_how_can_i_make_money_ask/)

Maybe a 4 steps approach to this:

- q1. What problems can I solve?
- q2. Is there a market/niche for it?
- q3. What's the average sale per unit/service sold?
- q4. What's the MVP (minimum viable product) I can launch and test the above?
- Why isn’t someone already doing it?
- What's the demand for your product/service, and how do you know there is one?

https://www.reddit.com/r/Entrepreneur/comments/evzxqv/dont_ask_yourself_how_can_i_make_money_ask/fg0f1lu/



weird seo hacks https://www.reddit.com/r/television/comments/cj7fr0/boris_johnson_last_week_tonight_with_john_oliver/ The fact that he might've told that wierd "i paint miniature buses" story to get on google search when people google "boris Johnson's bus" is genius — this is great.

Another SEO hack https://twitter.com/danbarker/status/1201280092665794560



good 404 https://www.reddit.com/r/webdev/comments/cj6stj/great_404_page_in_my_opinion/evcct67/

It's not though. Good error page design includes actionability and information, usually:

Search
Link to homepage
The actual status code (usually smaller)
Take a look at the NYTimes, I especially like their copy: "We’re sorry, we seem to have lost this page, but we don’t want to lose you."



https://www.reddit.com/r/bigseo/comments/evbhim/people_often_say_keyword_cannibalization_is_bad/

 canibalization notes



https://www.searchenginejournal.com/guest-post-manual-actions/351692/ google going against guestposts







https://www.reddit.com/r/TechSEO/comments/ewokif/rankings_dropped_after_migration_what_went_wrong/ don't change your website, theme... if you don't have to, this pairs nicely with the plan to "abolish" themes



https://www.reddit.com/r/SEO/comments/era30y/possible_reasons_for_google_rankings_disappearing/ loosing rank, algo



https://twitter.com/SaraSoueidan/status/1152221145464221696 rss





https://twitter.com/LinkiCZ/status/1152118954606964736 Blogy na http://wordpress.com nejdou crawlovat, pokud je váš user-agent Screaming Frog SEO Spider. Hodí se vědět při hledání odkazových příležitostí.



https://ismyhostfastyet.com/ goddady pagebuilder rychlejsi ako vsetky ostatne systemy.



https://www.vice.com/en_us/article/qjdkq7/avast-antivirus-sells-user-browsing-data-investigation avast jumpshot ahrefs

https://www.abc.net.au/news/2019-10-29/google-faces-accc-federal-court-misleading-use-of-data/11649356 jumpshot google personal data

https://www.reuters.com/article/us-avast-dataprotection/avast-pulls-plug-on-jumpshot-after-data-privacy-scandal-idUSKBN1ZT0R4

https://www.pcmag.com/news/the-cost-of-avasts-free-antivirus-companies-can-spy-on-your-clicks



https://twitter.com/webarx_security/status/1222851960023519233 98% of security are plugins



https://www.vox.com/the-goods/2019/3/19/18262556/face-mask-air-filter-pollution-vogmask-airpop + https://www.reddit.com/r/ABoringDystopia/comments/ewnur3/fashion/ As air pollution gets worse, a dystopian accessory is born



https://trends.google.com/trends/explore?geo=CZ&q=seo,ppc,cro je seo mrtvé?

https://hz.cz/cz/napoveda hosting zdarma

https://knowyourmeme.com/photos/1492929-skipping-steps how people aproach SEO

https://twitter.com/semrush/status/1216348605721796608

http://reviewskeptic.com/

https://twitter.com/JoyanneHawkins/status/1217149078838108160 always tag  gtm

https://www.semrush.com/blog/guide-to-biggest-seo-myths-on-web/

cqts



https://optinmonster.com/strategic-seo-tips-from-the-experts/

https://domaindata.cz/commoncrawler/?domain=404m.com&keyword=
https://backlinko.com/email-outreach-study
https://ahrefs.com/blog/google-disavow-links/

https://www.semrush.com/blog/weekly-wisdom-bartosz-goralewicz-crawl-budget/

https://www.evisions.cz/blog-2019-01-24-infografika-podil-vyhledavacu-google-a-seznam-na-ceskem-internetu-2019/ seznam

https://github.com/godaddy-wordpress/go theme

https://text-machine-lab.github.io/blog/2020/bert-secrets/ machine learning

https://allthelatest.net/recaps/the-week-in-digital-marketing-so-far-january-17th-2020/ ff

https://dejanmarketing.com/january-2020-core-update/ ai and updates to core algo

https://www.thedrum.com/opinion/2020/01/16/has-journalism-finally-found-its-big-tech-ally-inside-microsofts-pitch-publishers don't discunt microsoft and ddg

https://www.jumpshot.com/blog/google-hotels-industry-domination-key-takeaways-for-travel-ecommerce/ hotel

http://reviewskeptic.com/ hotel

https://www.jumpshot.com/resources/#ebooks terrible website

https://twitter.com/cptntommy/status/1218121712543457280 http3 for SEO

https://blog.cloudflare.com/http3-the-past-present-and-future/ http3

https://backlinko.com/on-page-seo decent

https://news.ycombinator.com/item?id=22110004 use ppc

https://localmarketinginstitute.com/google-my-business-ranking-factors/ gmb ranking factors

https://www.brightlocal.com/webinars/the-state-of-local-search-in-2020/ gmb



https://twitter.com/robertstipek/status/1219916416016699393 seznam

https://twitter.com/IvoKylian/status/1219961733596569601



https://linki.cz resource (cz)

https://moz.com/learn/seo/one-hour-guide-to-seo resource

https://www.hobo-web.co.uk/seo-tutorial/ resource

https://seocharge.org/seo-audit/ resource

https://moz.com/beginners-guide-to-seo resource beginners 

https://moz.com/blog/how-to-learn-seo resource beginners 

https://mangools.com/blog/learn-seo/ resource beginners 

https://www.quicksprout.com/seo/ resource beginners 

https://neilpatel.com/wp-content/uploads/2018/10/Neil-Patels-Advanced-Cheatsheet-to-SEO.pdf bad resource 

https://www.theatlantic.com/magazine/archive/2020/03/the-2020-disinformation-war/605530/ resource, trump = no more screaming, more whispering





https://support.google.com/webmasters/answer/35291?hl=en



https://cxl.com/blog/scale-organic-traffic/ less is more 





memes:
https://knowyourmeme.com/photos/1630290-phil-swift-slaps-on-flex-tape
https://imgflip.com/memegenerator/198448671/Phil-Swift-Slapping-on-Flex-Tape


https://rajgoel.github.io/reveal.js-demos/chart-demo.html#/











https://backlinko.com/google-ctr-stats
Organic CTR for positions 7-10 is virtually the same. 



https://www.reddit.com/r/SEO/comments/ercvku/seo_year_in_review_2019/ 



social buttons (shareability)

free tools
https://app.buzzsumo.com/discover/trending trending topics v USA, dobré na hladanie influencerov na twitteri
https://en.ryte.com/free-tools/topic-explorer/

appeal to authority (use steve jobs about simplicity)



mozcon recap

https://kwasi.com/15-seo-questions-mozcon/d





https://wordpress.org/support/topic/can-you-add-g-analytics-code-to-this/#post-12441723 33k a year, plugin with 5M users



# resources



https://twitter.com/navolnenoze/status/1156126574078582784 swiss knife



https://www.semrush.com/academy/courses/semrush-site-audit-course

https://www.buildersociety.com/forums/digital-strategy-crash-course.25/ <<<





https://docs.google.com/presentation/d/1-BSI41CyTyjZnW6FeT5HLXzT2zLIzK_LjRPuEy-JBaA/edit#slide=id.g45e4c6d6ba_0_19

https://www.searchlogistics.com/case-studies/sports-betting/ **case study, decent show of how to make SEO focused website**

https://twitter.com/LoukilAymen/status/1199304617726816257 how to check for broken links after migration using screamingfrog and archive.org

https://www.contentharmony.com/blog/content-hubs/ good images of content hubs

https://withcandour.co.uk/blog/episode-35-big-site-seo-with-andrew-smith big vs small seo

https://seoslides.page slides from SEO confs



https://parsers.me/us-court-fully-legalized-website-scraping-and-technically-prohibited-it/



R/juststart

https://www.reddit.com/r/juststart/comments/czb1d6/case_study_grow_site_from_300mo_to_1kmo_within_12/

https://www.reddit.com/r/juststart/comments/exlnmq/case_study_100k_niche_website_month_1/

https://www.reddit.com/r/SEO/comments/evkknp/case_study_invested_17195_298_hours_on_a_blog_in/

https://www.reddit.com/r/juststart/comments/en1iqc/fatstacks_30k_per_month_mostly_from_ads_anyone/

https://www.reddit.com/r/juststart/comments/ev365q/case_study_niche_site_0_i_forgot_it_existed_and/

https://www.reddit.com/r/juststart/comments/9725th/bprs07_case_study_month_17/

https://www.reddit.com/r/Entrepreneur/comments/emuaa9/fake_gurus/
https://www.reddit.com/r/Entrepreneur/comments/et9uwh/we_just_created_a_megaguide_to_saas_marketing/

https://www.reddit.com/r/juststart/comments/erf4id/authority_hackers_healthambitioncom_massive_seo/

https://www.youtube.com/watch?v=QpDYfrdoz_o



natural language API

https://cloud.google.com/natural-language/#natural-language-api-demo



some of the issues and myths

https://www.semrush.com/blog/guide-to-biggest-seo-myths-on-web/ 



rank math shill

https://www.reddit.com/r/juststart/comments/emtmw6/good_optimized_content_tips_to_help_start/fdvcqrd/?context=3

https://www.reddit.com/r/juststart/comments/ei22n1/starting_my_journey_month_0/fdvglv9/?context=3



trends

https://wordlift.io/blog/en/seo-trends/

https://www.rankranger.com/blog/serp-news-january2020



get some stock text from

blabot.cz







https://www.pathinteractive.com/blog/seo/5-changes-to-googles-search-quality-guidelines-in-september-2019/ raters



ideas vs. execution

https://www.reddit.com/r/marketing/comments/emvn1r/the_myth_of_unique_startup_ideas/



UX

https://littlebigdetails.com/page/2



- Make a simple brand (koloseo)
- Unified social media presence





## 2020

https://en.ryte.com/magazine/seo-new-years-resolutions-2020

https://twitter.com/buffer/status/1215953250513539073

https://www.kevin-indig.com/15-experts-reveal-the-trends-for-technical-seo-in-2020



### Feature addiction

https://news.ycombinator.com/item?id=22335738

https://cutle.fish/blog/12-signs-youre-working-in-a-feature-factory SEO plugins are feature factories

https://en.wikipedia.org/wiki/XY_problem people want X from SEO plugin, but never ask how much Y impact it has.





Don't use WordPress.

start with a purpose, you either have an idea, or a product

https://techcrunch.com/2011/10/19/dropbox-minimal-viable-product/
http://emoj.li/

https://pacholek.com/









### UX

footer "have something to say"

https://typora.io/



Design for user.

Are you visiting frequently? speed.

Are you trying to impress? Design.

Are you god? Both.

Speed is important if you are selling stuff.  https://web.dev/value-of-speed/

![Chart: mobile load time versus relative mobile conversion rate](/home/lebaux/pCloudDrive/notes/media/mobile-versus-relative-chart.jpg)





### Talks

https://www.slideshare.net/DawnFitton/planning-an-seo-strategy-for-a-new-website-smxl-milan-2019/

https://docs.google.com/presentation/d/e/2PACX-1vQF5TXZaRZA8NsGZYnHNuFBlI7HQIPA-Hf23b3S2YLRtoZNn2EYmcqqk9kz5YvAyH28g1n1FzTVHhQO/pub?start=false&loop=false&slide=id.g56b06f91d5_0_35



### Videos

https://twitter.com/MarketingMiner/status/1168780917340438528 content gap or Podstavca, pretty dope

https://twitter.com/dejanseo/status/1165862897978757120 link detox

https://www.youtube.com/watch?v=iBOFJxbL3Io

https://www.youtube.com/watch?v=n8yZqOJ6CZk

Technické SEO na WordPressu od A do Z od Pavel Ungr - WordCamp Praha 2019



dle meho top prednaska, nejmensi viditelnost

https://www.youtube.com/watch?v=0LSRdjuXJAQ









### WP sites examples (good + bad)

template-y, could use improvement:

- https://marijanakay.com/
- https://slagtermedia.nl/wat-wij-doen/

- https://www.rockawaycapital.com/en/
- https://www.nuvio.cz/ (missing menu?)
- https://lynt.cz (hope he will not kill me)

Good:

- https://www.elated.com/html5-audio/ (breadcrumbs, tags, internal linking)
- https://www.mytimi.cz/ (not wp)
- https://ottobohus.cz/ (drupal, older site, pretty timeless design but ignores #cookies
- https://www.nevychova.cz/ (social proof, good desc)
- https://ventrata.com/ better, not great, but better
- https://withcandour.co.uk/ great website using only fonts and colour



https://bremer.immobilien/











### Gamification = bad

https://www.reddit.com/r/SEO/comments/dvnbba/my_website_has_51_spam_score_according_to_moz_the/

https://www.reddit.com/r/Windows10/comments/dwepfy/i_like_this_my_search_rewards_in_search/ microsoft doing it

https://www.reddit.com/r/webdev/comments/evdgun/pretty_proud_of_this/

https://website.grader.com/results/theseoframework.com

https://twitter.com/thezilshaveeyes/status/1216631641671716865 gamification = good.

https://twitter.com/LucGodfroid/status/1201596578076733441 gamification = good

https://twitter.com/micheile010/status/1197230246258495511 gamification = good

https://twitter.com/pierrefar/status/1159084803158958081

https://twitter.com/izuddinhelmi/status/1151035508295962628

https://twitter.com/Jannerboy1962/status/1145921290689294336

https://twitter.com/FarrelNobel/status/1145931065715900416



#### WordPress own site health check

https://make.wordpress.org/core/2019/04/25/site-health-check-in-5-2/

https://core.trac.wordpress.org/changeset/46106

https://core.trac.wordpress.org/ticket/47046#comment:7 (our TSF feedback that went into core of wp health check)



https://behavioralscientist.org/expertise-in-the-age-of-youtube/

This isn’t limited to competitive games. The same logic—tie your identity to a number, do whatever you can to make that number go up—is part of the psychological engine behind social media. This article is not the place to discuss the pros and cons (or cons and cons, perhaps) of that particular dynamic. But it’s enough to observe that likes, shares, and comments provide their own form of the rapid and quantified feedback found “high-validity” environments.



## Tools

### SEO

Crawlers: ScreamingFrog, SiteBulb, DeepCrawl, SEO Macroscope

http://www.backlinkwatch.com/index.php

https://www.semrush.com/blog/a-guide-to-effectively-using-googles-new-speed-report/ you do not need that (WOW score)

https://tld-list.com/

https://seolyzer.io/

https://surferseo.com/

https://alsoasked.com/

https://explodingtopics.com/

https://twitter.com/ErSurajShukla1/status/1215871926750416896 

https://explodingtopics.com/

https://trends.google.com/trends/explore?q=N26

https://en.ryte.com/free-tools/topic-explorer/ (english only)

https://algoroo.com/

https://backlinkshitter.com

https://www.wolframalpha.com/input/?i=words+that+contain+press for finding names

https://supple.com.au/tools/nofollow-ugc-sponsored-bookmarklet/

https://www.goinflow.com/cruft-finder-seo-tool/ find bad websites

https://www.goinflow.com/index-bloat/

https://www.goinflow.com/cruft-finder-seo-tool/

https://backlinko.com/seo-expert resource

https://saijogeorge.com/json-ld-schema-generator/faq/

https://twitter.com/distilled/status/1215634503999291399 tech seo audit checklist

https://docs.google.com/spreadsheets/d/19dHdC3vfoC0_fJE57JqoKUlaVHZxyAfnkruiKDDIMtY/edit#gid=316387007 seo checklist (most buzzwords included)

https://ftf.agency/tools/linkbuildr/ Linkbuildr is a **FREE WordPress Plugin** that automatically sends recently published content to relevant influencers for promotion.

https://www.aleydasolis.com/en/search-engine-optimization/how-use-google-spreadsheets-for-seo-tasks/

https://google-webfonts-helper.herokuapp.com/fonts ([self host google fonts](https://news.ycombinator.com/item?id=22394524))





### Figuring out name

https://namechk.com

https://tld-list.com

https://domainr.com

https://www.powerthesaurus.org

https://keywordtool.io

https://keywordplanner.net



## Making a simple WordPress website without any plugins

1. Download WP 5.3 and Theme 2020 https://github.com/WordPress/twentytwenty
2. Basically follow https://wordpress.org/support/article/twenty-twenty/
3. Install and activate
4. Set name, description, disable widgets and set theme menus



### Copy for example website

- Stavíme SEO, které vydrží dlouho.
- Stavíme SEO, které odolá zubu času a vydrží přitahovat návštěvníky.
- Stavíme SEO, které vám bude přitahovat zákazníky dlouho po tom co vám vystavíme poslední fakturu.
- Všichni říklají, že SEO se furt mění ale naše weby stojí do dnes.
- SEO které se nesesype za pár let není mýtus.
- Velké SEO pro malé hotely.
- Stavíme SEO, které do vašeho hotelu přitáhne návštěvníky a jen tak se nesesype.
- Stavíme SEO weby, které do vašeho hotelu přitáhnou návštěvníky a jen tak se nesesype.
- Postavíme vám SEO web, který do vašeho hotelu přitáhne návštěvníky a bude stát za to roky po té, co vám vystavíme poslední fakturu.
- Postavíme vám SEO web, který do vašeho hotelu přitáhne návštěvníky a nezboří vám pokladnu.
- Naším chlebem je pomoct najít vaším hostům cestu do vašeho hotelu.
- Naším chlebem je přitáhnout do vašeho hotelu více hostů.
- Naší prací je přitáhnout do vašeho hotelu více hostů.
- Uděláme z vašeho hotelu kolos, který bude vidět napříč internetem.



Copy:

- https://cotton-broker.com/
- https://flocc.co/
- https://filburg.co/
- http://wordminds.com/
- https://www.theharbour.be/
- https://www.pancakeapp.com/



2020 theme:

https://marko.fyi/firefox/

https://sethgoldstein.me/are-wordcamps-dying/

https://en.ryte.com/magazine/seo-new-years-resolutions-2020







# Google = bad boi

https://twitter.com/LaurenJohnson/status/1217259867343413250 google removing 3rd party cookies.

https://moz.com/blog/clickstream-data-revolution 

https://www.cookiestatus.com/ + google 3rd party cookie blocking.

https://blog.innerht.ml/google-yolo/?reveal

https://www.seroundtable.com/google-html-sitemaps-seo-28312.html

https://www.reddit.com/r/ProWordPress/comments/ewt21e/you_have_2_weeks_to_test_this_feature_plugin/

https://news.yahoo.com/facebook-google-amazon-apple-state-lobbying-170525755.html

https://www.ted.com/talks/eli_pariser_beware_online_filter_bubbles?language=en#t-515564

https://www.theverge.com/2020/2/6/21126858/google-chrome-80-browser-notifications-ads-block-how-to heavy ads

https://www.tourism-review.com/google-accused-of-unfair-competition-again-news11397

https://google-ml.blogspot.com/2020/02/google-accused-of-competition-abuse-in.html

https://www.theregister.co.uk/2020/02/20/chrome_deploys_deeplinking/

https://twitter.com/RossHudgens/status/1123069919300599808 **super important to connect it to last years stats I already have here** 

https://twitter.com/Fearless_Shultz/status/1142446393971417088





https://en.wikipedia.org/wiki/Brussels_effect this is what we need to hope for

https://www.reddit.com/r/europe/comments/ewkyd1/european_parliament_has_voted_in_favour_of_a/



https://www.youtube.com/watch?v=6xSxXiHwMrg ad



### AMP



https://www.reshoper.cz/datablog/studie-amp-hlasi-u-ceskych-e-shopu-narust-o-300-je-cas-ho-nasadit/ úplne dole o amp

https://twitter.com/Faborsky/status/1216671013053915136 **the amp garbage at the bottom.**

https://www.reddit.com/r/webdev/comments/803r4c/amp_the_missing_controversy/

https://www.polemicdigital.com/google-amp-go-to-hell/

https://9to5google.com/2019/04/18/apple-mozilla-google-amp-signed-exchanges/ kradnutie URL

https://ferdychristant.com/amp-the-missing-controversy-3b424031047

https://www.youtube.com/watch?v=igRCgYW2aus

https://twitter.com/JohnMu/status/1219937311024087040

https://news.ycombinator.com/item?id=22107823

https://www.ietf.org/about/

https://techcrunch.com/2020/01/02/google-has-little-choice-to-be-evil-or-not-in-todays-fractured-internet/

https://www.ietf.org/about/ this is how you should do a standard

https://twitter.com/frontity/status/1222196690981195776

https://www.bleepingcomputer.com/news/google/google-chrome-tests-replacing-urls-with-search-queries-in-address-bar/ wut

https://9to5google.com/2019/04/18/apple-mozilla-google-amp-signed-exchanges/

https://www.reddit.com/r/marketing/comments/evl4n2/help_0125_website_conversion_were_an_ai_automl/ don't make a new website unless you have problems

https://www.nytimes.com/2018/02/20/magazine/the-case-against-google.htmlgith

https://twitter.com/michaeltieso/status/1155811748391211008 Implementing PWA was significantly easier than AMP

vs

https://twitter.com/machal/status/1140938462616576000

https://www.christianoliveira.com/blog/seo/amp-stories-seo/ amp stories have VIP in SERP

[According to Gary Illyes](https://courses.mariehaynes.com/search-news-you-can-use/episode-26/), if you have AMP pages, Google will only use them for indexing decisions if they are declared the canonical version of that page. https://www.mariehaynes.com/mobile-first-indexing/#How_do_AMP_pages_play_into_MFI



### Bad google results

Tools to Monitor Website Changes

free brand monitoring tools

best brand monitoring tools https://www.codefuel.com/blog/top-10-brand-monitoring-tools-2016/ broken images

https://reportgarden.com/social-media-brand-monitoring-free-tools/

#duckduckgo

https://duckduckgo.com/?q=list+of+two+factor+authentication+companies&t=vivaldi&ia=web

find that bing article when it says bing is loosing to DDG on mobile accorind to google

https://www.youtube.com/channel/UC_E4px0RST-qFwXLJWBav8Q/featured business casual youtube channel







use google simulator:

obchod kvetiny zlin

domaci potreby zlin (facebook result 4th)

https://www.brightlocal.com/local-search-results-checker/



https://www.google.com/search?q=andelova zlin ordinacni hodiny













### Forbes

https://www.seroundtable.com/forbes-google-search-traffic-28912.html

https://www.forbes.com/sites/forbesagencycouncil/2020/01/28/12-top-content-marketing-trends-just-over-the-horizon/

https://digiday.com/media/google-reveals-sites-failing-ads-including-forbes-la-times/

search for forbes annoying ads

https://thefuturebuzz.com/2019/07/23/forbes-is-a-website-thats-basically-unreadable-at-this-point/



https://www.reddit.com/r/selfhosted/comments/eq4l9p/nextcloud_hub_launches_to_compete_directly_with/feoe02z/ the guest network

https://www.forbes.com/sites/gordonkelly/2020/02/08/windows-10-warning-serious-failure-provokes-questions-and-anger/#7eb5ac7d6a52 bold claims against ms

https://youtu.be/64xcgvEJ3Ys?t=566 how scammer used the forbes reputation













# Gates Yacht

https://twitter.com/zsk/status/1226857259747151883 pressure to deliver news fast, not accurately

Wrong: 

- [Telegraph (started rumour) 08.02.2020](https://www.telegraph.co.uk/news/2020/02/08/bill-gates-becomes-first-buy-500m-hydrogen-powered-super-yacht/)
- Guardian
- Microsoft News (MSN)
- Mashable
- Engadget
- Metro
- The Sun
- DailyMail
- New York Post
- Unilad
- Androidpit
- blesk.cz
- zive.cz
- seznamzpravy.cz
- refresher.cz
- eurozpravy.cz
- dennikn.sk





https://www.novinky.cz/koktejl/clanek/zenu-stihl-trest-za-odhozenou-papirovou-tasku-az-po-11-letech-40314865 nikto nikoho necituje (ako backlinky nefungujú)



# Blogs and twitters to follow (seo/nonseo)

https://www.danielnytra.cz/google-data-studio-nastaveni/

https://baremetrics.com/blog

https://twitter.com/LinkiCZ

Aleyda Solis newsletter/twitter

onely

http://www.seobythesea.com/2019/09/category-diversity/

https://web.dev/value-of-speed/ how to measure speed impact on your conversion rat using GA

https://www.portent.com/blog/seo/developers-seo-guide.htm







# Mofo website

http://motherfuckingwebsite.com/

http://bettermotherfuckingwebsite.com/

https://bestmotherfucking.website/







### Pacholek.com

https://twitter.com/drahomiraba1/status/1168473182141079553 (+search

)



### Themes

https://minimal-blog.lekoarts.de/ (react+gatsby, minimal, fast AF)

https://twitter.com/alexadark/status/1156620043175833602 gatsby + wp

https://libra.org/en-US/ not a theme per se, but wp + gatsby

https://javascriptforwp.com/conference/ most about js and wp





# Cut content

https://moz.com/blog/google-review-likes what review likes do? This article is good start but really needs more data.

John O'Nolan, Founder of Ghost  https://twitter.com/JohnONolan/status/1233024423206113281 and WP drama https://twitter.com/JohnONolan/status/1146075173134618625



disqus https://twitter.com/autiomaa/status/1138118026103070720

Google Cloud Natural Language API https://twitter.com/PavelUngr/status/1116708563613888512



https://www.semrush.com/blog/how-to-build-winning-creative-analysis-over-10-million-native-ads/ i hate this because it is all over the place, but ye, use photos, not illustrations (stats)



https://www.brightlocal.com/research/google-my-business-insights-study/#growth inconclusive (stats)



SEO PLUGINS AND REVIEWS

To help save you time, we‘ve whittled down those tens of thousands of plugins into a list of the 71 best WordPress plugins for a huge variety of different uses. 71 might still be a lot, but it‘s better than 54,000! https://twitter.com/kinsta/status/1148237610286026755 Tweets like this make or break a wordpress plugin.



7 Best Alternatives to Yoast SEO WordPress Plugin via 
@AdamHeitzman
: https://searchenginejournal.com/yoast-seo-alternatives/

https://twitter.com/sejournal/status/1143879357196374016





### Other

https://twitter.com/thepatwalls/status/1231958142457507842 how hard it is to get 1000 MRR

https://twitter.com/dsottimano/status/1230708609744375809 there are other search engines, bing is gaining (weak source)

https://twitter.com/RichTatum/status/1218169476132364289 weak numbers to be conclusive

https://twitter.com/BrianClifton/status/1166970747857887232 stop tracking scroll depth in GA

https://twitter.com/glenngabe/status/1215256623876362241 collegeHummor downfall in SEO (road to be profitable - they havebt been for 20 years!) now they 301 to their youtube channel, completely losing because of algos.

https://twitter.com/elonmusk/status/1188889708266315776 it is hard to google tesla solar accoring to elon





### Fish

https://twitter.com/randfish/status/1153415625923948549

https://twitter.com/randfish/status/1149765033758298112

https://twitter.com/randfish/status/1149017305134837760

https://twitter.com/randfish/status/1148311596235554816

https://twitter.com/randfish/status/1143419355863977986

https://twitter.com/randfish/status/1141446750785298432





# Tags

`#seo ` `#wordpress` `#tsf` `#wordcamp`