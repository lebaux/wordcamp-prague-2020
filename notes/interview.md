# Otázky na řečníka

1. **Proč jste se rozhodli pro WordCamp Praha?**

   Pretože mi pražský WordCamp pred 4 rokmi úplnou náhodou zmenil život. Trochu as rozpíšem, keďže WordPress je o tom blogovaní. Ak len chcete vedieť, o čom bude moja prednáška, skočte na otázku číslo 2.

   Písal sa rok 2016 a podobne ako [Vladimír Smitka](https://lynt.cz/) som bol ajťák, čo prerástol do marketingu. Bolo bežné, že som sa jeden deň modlil k MacBooku nech sa pripojí k tlačiarni... a na druhý deň som robil typický malý WordPress webík s eshopom, platobnou bránou, SEOm, nejakou tou analytikou a sliderom s 30 animáciami. Všetko v 4 jazykoch, na vlastnom serveri. A hlavne bezpečne, rýchlo a lacno. A tie Facebooky tiež.

   WordPress umožňuje bežným ľudom robiť relatívne komplexné projekty a byť svojim pánom. Tie prvé weby a experimenty často vyzerajú ako keď sa niekto snaží postaviť IKEA nábytok bez návodu... ale sú vaše. A plnia svoju funkciu. Pričuchnete si k open source, základom programovania. Veľa ľudom to naštartovalo kariéry. Je to úplne iné ako korporát. 

   Môžete začať podnikať, rozbehnúť nápad a mať svoje miesto na Internete. Táto sloboda pre mňa bola vždy najväčší magnet. Chcel som do toho viacej preniknúť a na to nie je nič lepšie ako spoznať komunitu.

   [Vladislav Musílek](https://musilda.cz/) vtedy vyhlásil súťaž o lístok a ja som vyhral. Tak som išiel. Neočakával som žiadne zázraky od komunitného eventu zameraného na redakčný systém. V tej dobe som robil pre jednu z najlepších eventových agentúr v ČR, takže som mal s čím porovnávať.

   Samozrejme organizátori mi vybili sklíčka na okuliaroch. Nie je ťažké poznať, keď niekto niečo robí s láskou, nie s vidinou zisku. 

   To najlepšie boli samozrejme ľudia. Účastníci, prihovorte sa k ostatným. Nie len ku speakerom. Ja som z publika spoznal Adama Laitu, ktorý sa od tej doby potiahol pražský WC o ďalšie dve úrovne hore. Už 4 roky mu dlžím fľašku rumu, tak som si povedal, že je na čase sa vrátiť na miesto činu a odškrtnúť to z todo listu.

   Atmosféra pražského WC priťahuje aj ľudí, čo robia úplne iné veci ako WP. Ja som napríklad spoznal [Vítka Ježka](https://www.vitekjezek.com/), čo založil skvelý projekt [Rekola](https://www.rekola.cz/). Dokonca mi aj ponúkol prácu. Takže pokiaľ ešte nemáte lístok a ste 50/50 či ísť na tohtoročný WordCamp, úprimne odporúčam prísť.

   To bolo pred 4 rokmi.

   Bolo mi jasné, že chcem pracovať full-time s WP. V tej dobe sa jeden bláznivý Holanďan rozhodol postaviť plugin konkurujúci najväčším pluginom vôbec, The SEO Framework. Hovoril som si, že by mohol potrebovať markeťáka a tak som sa k nemu pridal.

   Teraz sa vraciam na miesto, kde to všetko začalo, tentokrát už ako člen ostrieľaného vývojarského tímu. Naučili sme sa hromadu tvrdých lekcií o podnikaní vo WP, užívateľoch a hlavne o SEO. Moc ďakujem organizátorom, že nás vybrali. Je to po prvé, kde s The SEO Framework vystúpime.

1. **O čem bude tvá přednáška?** 

   Časťou mojej práce je sledovať zmeny v SEO, čo v praxi znamená tráviť 2-3 hodiny denne na Twitteri a čítaním článkov. Potom riešime, či tie zmeny ovplyvnia užívateľov nášho SEO pluginu, či musíme niečo doprogramovať alebo zmeniť.

   Úprimne? Tých informácií je strašne moc a väčšina je šum. Občas mi to príde ako zámer, aby SEO experti mohli hovoriť aké je to všetko zložité. Google to vyhovuje, lebo môžu v tom hluku prepašovať desiatky malých zmien, ktoré slúžia im a nie tak úplne vlastníkom webov.

   Ak spravujete pre niekoho web a má zároveň Google My Business (alebo AdWords), zamestnanci Google vašim klientom priamo začnú volať a ponúkať im SEO. To sa už deje v USA. Mne nepríde moc cool, že Google vlastní vyhľadávanie a zároveň ľudia s emailom @google.com oslovujú vlastníkov malých webov a firiem.

   Takýchto chuťoviek je viac. Na tomto WordCampe bude prednáška o tom aký je Google AMP skvelý. Odo mňa budete počuť prečo si myslím, že je to rakovina ktorá ohrozuje slobodný Internet, takže aj WordPress.

   Nebojím sa hovoriť o nepríjemných témach. Som hrdí, že sme nezávislí vývojári. Neprišli sme do Prahy nič predávať, cieľom mojej prednášky je vám pomôcť s problémami, ktoré žiadny SEO plugin nevyriešiť nemôže ani keby chcel.

   

1. **Proč by lidé měli přijít?**

   Chcel by som, aby typický malý český WordPressák s 1000 problémami a 1 rozpočtom zorientoval v SEO. Spomeniem najlepšie SEO nástroje zadarmo a kde sa oplatí zaplatiť. Zároveň si povieme, čo robiť nemusíte (a užívatelia s tým stále strácajú čas).

   

1. **Jak to funguje se SEO pluginem, stačí jej nainstalovat a uživatel má poté vyhráno?**

   SEO pluginy globálne hrajú malú úlohu. Kúpte si starú doménu ktorá ma hromadu spätných odkazov a máte náskok, ktorý vám nedá žiadny plugin. Vyhrať v SEO je každým rokom ťažšie, koniec koncov o tom bude moja prednáška.

   

1. **Jak vypadá vývoj vašeho SEO pluginu? Co jsou/byly největší výzvy?**

   Vývoj WordPress pluginu je na prednášku samú o sebe. Pluginy a šablóny sú dôvod, prečo je WP tak populárny a pritom má celý tento ekosystém pred sebou veľké... výzvy. Poviem len toľko, že najväčšie problémy vývoja nie sú programátorské, ale politické a obchodné.

   Osobne sa nesťažujem, ale možno ste si všimli, že 2 vývojári obrovských (AIO SEO + GADWP) pluginov hodili flintu do žita a predali ich človeku, ktorý rieši len zisk.

   Urobte si obrázok sami. Nie všetko na WP je skvelé.

   

1. **Jaké jsou nejčastější dotazy od uživatelů pluginu?**

   Z technického hľadiska, je to hlavne kompatibilita medzi pluginami. To rieši Sybre. Trochu mu prihrejem polievočku — často v tomto procese našiel v pluginoch obrovské bezpečnostné diery. Veľa vývojárom stačilo v tichosti napísať mail a vyriešili to, čo je super. V iných prípadoch boli nebezpečné pluginy vyhodené z repozitára WordPressu... aj tak sa dá riešiť kompatibilita.

   Najviac som hrdý na dieru, [čo našiel Sybre v Yoast SEO a bola tam 8 rokov od verzie 1](https://wpvulndb.com/vulnerabilities/9445). Našli sme ju len preto, že jednému užívateľovi niečo chýbalo v Yoast SEO a chcel prejsť k nám. Pre poriadok musím povedať, že s tímom Yoast máme dobré vzťahy. Vidíme SEO inak ako oni a naše doťahovanie o tom, kto má lepší plugin len pomáha užívateľovi. Zdravá konkurencia.

   Druhý typ dotazov sú požiadavky na nové funkcie (fíčury). V SEO je veľa trendy vecí, ktoré Google predstaví z dňa na deň. Nikto už nerieši, koľko dá práce to naprogramovať len aby to Google prestal zobrazovať vo výsledkoch vyhľadávania. Sme v tomto konzervatívni a pokiaľ je daná novinka zbytočná pre 99% užívateľov, nepridáme to. Alebo nám to trvá, lebo to chceme spraviť správne. Je to strašný marketing, ale nikdy neuspokojíte každého.

1. **Můžeš nám něco říci o posledních trendech v oboru SEO a jestli jsou aplikovatelné i na weby postavené na WordPressu?**

   Statické weby sú posledná hot technológia. Sú rýchle, bezpečné a technologicky o dekádu pred WP. Myslím, že Matt to chápe a má pre WP plán. Z pohľadu SEO pluginov, všetci implementujeme predpoklady na to, aby ste mohli mať WP v headless režime. Napríklad kompatibilitu s WPGraphQL. Myslím, že v tomto roku uvidíme prvé kompletné šablóny pre koncových užívateľov postavené na React.js.
   
   Ak neviete o čom tu točím, porovnajte si rýchlosť:
   
   - [klasická twenty-nineteen šablóna](https://wp-themes.com/twentynineteen/)
   - [twenty-nineteen šablóna postavená na React.js](https://twentynineteen.frontity.org/)
   
   Pre koncových užívateľov sa toho do budúcna moc nemení, len že WP bude o moc rýchlejší (a zložitejší).
   
   


8. **Která kritika Tě v životě výrazně posunula vřed?**

   Každá, veľa vecí robím zle. 



9. **Co bylo největší dobrodružství Tvého života?**

   Ťažko povedať, na vysokej som zažil takú tú klasickú divočinu, ale aktuálne žijem na dedine a najväčším dobrodružstvom je pestovanie rajčín, domáci sirup a tak. Starnem.



10. **Při jaké příležitosti jsi se naposled spontánně a zcela bez studu přestal ovládat?**

    Opýtajte sa ma na WordCampe. Ďakujem za rozhovor a vidíme sa v Prahe!